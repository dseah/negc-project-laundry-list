## Project Laundry List WebApp HTML Source

This is a static HTML shell for the Laundry List web app. The `index.html` file references `css/style.css`, and hopefully is straightforward. This was tested in Chrome browser.

The two javascript files are not essential; `holder.js` creates placeholder images, and `list.js` just reloads the browser page if it detects that the source file has changed.

### Theoretical Usage

There are two areas: a `#banner` area and an `#info` table. The banner is at the top, and has the eye-catching image. The info table is where all the data is displayed. The width is fixed arbitrarily at 600px, which is about as small as we can make it and have the typography we're using. If it needs to be narrower, then we need to shrink the information further.

**Banner** Rather than create a complicated composite image for the banner area as we originally thought, I'm just using backgrounds to make this easier to push out. After all, if there are just 5 different states, then these backgrounds could be photographic images that are simply provided by the client. This would allow for frequent changes, and for seasonal variations. The positioning of the text on the banner can be accomplished by adjusting the `top` and `left` properties if the images require it. 

**Info Table** This is arranged as a number of DIV columns (5, including the black spacer) that contain tables. The width calculations are all in pixels, unfortuantely, but they are defined in only one place so it should be straightforward to modify. This is commented in the CSS stylesheet.

### Image Assets

The images are png files but they don't need to be. They are set as background images for #banner in the stylesheet. There are five background images, named `bg1.png` through `bg5.png` in the `img` directory.

The socks are 24-bit transparent PNG files stored in the `img` directory. There are two brightnesses for each color for a total of four:

* `sock-now.png` is bright orange. `sock-now-dim.png` is the 50% opacity version (darker).
* `sock-next.png` is the bright gray color. Likewise, `sock-next-dim.png` is the 50% opacity version (darker).





